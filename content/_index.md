## What’s Provided?

[Theseus Cores](https://gitlab.com/theseus-cores/theseus-cores)

- FPGA source code
- Software to run with Gnuradio and RFNoC
- Unit tests and examples to confirm software builds and FPGA testbenches run

[Theseus Docker](https://gitlab.com/theseus-cores/theseus-docker)

- Provides docker images for UHD, gnuradio, and Vivado continuous integration testing, rebuilt weekly for stable branches
- Images published to [Theseus Cores Docker Hub](https://hub.docker.com/u/theseuscores)

[Theseus UHD Builder](https://gitlab.com/theseus-cores/theseus-uhd-builder)

- Helps build the RFNoC images by tagging compatible versions of theseus-cores and UHD FPGA source code
