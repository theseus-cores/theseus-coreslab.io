---
title: About Theseus Cores
comments: false
---

We started Theseus Cores because we noticed the FPGA world has a profound lack of *open-source* answers to common DSP questions, including…

- How do I channelize spectrum?
- How do I change sample rates?
- How should I modulate data?
- … etc

Many of these questions have been solved for over 30 years in common signal processing textbooks! But not in a way that users can run, test, and build for hardware. Why do developers need to constantly build (and rebuild) these utilities??

So this is why Theseus Cores lives: Theseus Cores provides open source DSP utilities and applications so that developers can better harness the computing power of modern FPGAs and heterogeneous platforms.
