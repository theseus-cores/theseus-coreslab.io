---
title: Introducing Theseus Cores
date: 2019-06-02
author: EJ Kreinar
---

I'm very happy to announce the (very modest) release of the Theseus Cores project.

Theseus Cores is designed to provide open source FPGA cores for digital signal processing and software defined radio, plus the means to USE the FPGA cores in real life.... In practice, that mostly means FPGA code propagates up through RFNoC blocks which have both UHD and Gnuradio software hooks for users to attach to. In the future it would be great to support other FPGA platforms if there's interest too.

So far, Theseus Cores provides the following RFNoC FPGA blocks and corresponding software:

- **Polyphase M/2 Channelizer**: A polyphase channelizer where each channel outputs 2x sample rate and is compatible with perfect-reconstruction. Thanks to Phil Vallance for re-implementing the channelizer described in his GRCon 2017 presentation-- it works!
- **"1-to-N" DDC Chain**: Parameterized instantiations of "N" independent DDCs, where each DDC is connected to the *first* input (a very basic, brute force channelizer). Note I've seen several mailing list discussions in the past year about 1-to-4 or 1-to-8 DDC channelizers -- this block provides the generalized version of that scenario.
- **DUC + DDC Rational Resampler**: A "hacked" rational resampler, consisting of a DUC and a DDC back-to-back. It's not pretty, but it can occasionally be helpful.

Furthermore, in an effort to TRY to create an open source FPGA project that doesnt catastrophically break on a regular basis, we've set up continuous integration tests for both software and FPGA. Dockerfiles are provided in a separate repo: [theseus-docker](https://gitlab.com/theseus-cores/theseus-docker). Theseus Cores also pushes tagged docker images for various versions of UHD and Gnuradio, where the branches for UHD-3.13, UHD-3.14, UHD's master, and gnuradio's maint-3.7 are rebuilt weekly. This may be of auxiliary use to people building UHD and gnuradio in a CI scenario: https://hub.docker.com/u/theseuscores

**What's next??** It's a modest list of features so far, but I'm sure we can all sympathize that things move slowly when developing FPGA code. Here's a quick rundown of a few ideas on the horizon:

- Arbitrary resampling
- Channel downselection for the M/2 channelizer (currently all channels must be output... it's far more useful to select a subset of channels to return and just grab those)
- Channel reconsonstruction *after* the M/2 channelizer (maybe)
OFDM receiver (maybe)

We need more ideas and contributors! Now that this thing exists, I would LOVE to see Theseus Cores fill itself out with some of the more common DSP utilities that really should be available as open-source... it would be absolutely amazing to provide a library of components and applications for FPGA developers in a similar way that gnuradio provides for the software community.

Please reach out with suggestions for relevant FPGA utilities or applications you'd like to see -- or even better, if you have ideas or code you're willing to share with the project! If you are interested in getting involved in any way, I would be happy to hear from you: info@theseus-cores.com
